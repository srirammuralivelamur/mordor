# coding=utf-8
"""utils package"""
__author__ = 'sriramm'

import sys

sys.dont_write_bytecode = True
from .document import MorDoc, _document_register

__all__ = ('MorDoc', '_document_register')