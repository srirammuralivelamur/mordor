# coding=utf-8
"""utils package"""
__author__ = 'sriramm'

import sys
sys.dont_write_bytecode = True
from re import compile, IGNORECASE, match
from datetime import datetime

EMAIL_REGEX_BASE = compile(
    '[a-zA-Z0-9._+-].*?\@[a-zA-Z0-9._+-].*?\.[a-zA-Z0-9._+-]{2,3}',
    IGNORECASE)

field_meta_members = ('unique_with', 'unique')


class BaseField(object):
    """Base field"""

    def __init__(self, *args, unique=False,
                 unique_with=None, **kwargs):
        """Base field init
        :type kwargs: dict
        """
        self._value = args[0] if args and len(args) == 1 else None
        self._meta = kwargs
        self._meta.update({
            key: val for key, val in locals().items()
            if key in field_meta_members})

        for key, val in self._meta.items():
            setattr(self, key, val)

    @property
    def value(self):
        """Value getter for base field"""
        return self._value

    @value.setter
    def value(self, val):
        """Value setter for base field
        :param val: Value to set for field
        """
        self._value = val

    def _validate(self):
        """Base validator"""
        return True


class StringField(BaseField):
    """Base string field"""

    def _validate(self):
        """String validator"""
        return True if self._value else False if self._meta.get(
            'required') else True if not self._value else \
            isinstance(self._value, str)


class EmailField(BaseField):
    """Base email field"""

    def _validate(self):
        """Email validator"""
        return True if self._value else False if self._meta.get(
            'required') else True if not self._value else \
            isinstance(self._value, str) and match(
                EMAIL_REGEX_BASE, self._value)


class DictField(BaseField):
    """Base dict data field"""

    def _validate(self):
        """Dict field validator"""
        return True if self._value else False if self._meta.get(
            'required') else True if not self._value else \
            isinstance(self._value, dict)


class ListField(BaseField):
    """Base list field"""

    def _validate(self):
        """List field validator"""
        self._value = list(self._value) if \
            isinstance(self._value, tuple) else self._value
        return True if self._value else False if self._meta.get(
            'required') else True if not self._value else \
            isinstance(self._value, list)


class DateTimeField(BaseField):
    """Base datetime data field"""

    def _validate(self):
        """Datetime validator"""
        return True if self._value else False if self._meta.get(
            'required') else True if not self._value else \
            isinstance(self._value, datetime)


class ReferenceField(BaseField):
    """Base reference field"""

    def __init__(self, *args, **kwargs):
        """Reference field init"""
        super(ReferenceField, self).__init__(*args, **kwargs)
        self._model = self._value if isinstance(self._value,
                                                str) else None

    def _validate(self):
        """Dummy reference field validator. Returns True for now"""
        return True


__all__ = ('StringField', 'EmailField', 'DictField',
           'ListField', 'DateTimeField', 'ReferenceField')
