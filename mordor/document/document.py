# coding=utf-8
"""utils package"""
__author__ = 'sriramm'

import sys

sys.dont_write_bytecode = True
from bson import ObjectId
from pymongo.cursor import Cursor
from pymongo import ASCENDING, DESCENDING
from re import compile, IGNORECASE
from mordor.utils import get_connection
from bson.dbref import DBRef

globals()['_document_register'] = ()


def get_query(args, kwargs):
    """Get query dict from args/kwargs
    :param args: Arguments tuple. Takes dictionaries for query
    :param kwargs: Keyword arguments tuple. Takes dictionaries for
    query
    """
    # Construct query from args tuple
    query = {}
    [query.update(x) for x in args if isinstance(x, dict)]

    # Update query if kwargs brings in query parameters
    query.update(kwargs)

    # Update query key/value to replace 'pk' with '_id' and
    # value to use ObjectId for query
    query = {'_id' if key in ('pk', 'id',) else key: ObjectId(
        val) if key in ('pk', 'id',) else val for key, val in
        query.items()}

    return query


def generate_mongo_query(query_dict):
    """Generate mongo query dict
    :param query_dict: Query dictionary to generate PyMongo query dict
    """
    updated_query = {}
    for key, val in query_dict.items():
        key_split = key.split('__')
        if len(key_split) == 2:
            if key_split[1] in ("contains", "icontains"):
                regex = ".*?%s.*?" % val
                regex = compile(regex) if \
                    key_split[1] == "contains" else \
                    compile(regex, IGNORECASE)
                updated_query.update({key_split[0]: regex})

            elif key_split[1] in ('starts_with', 'istarts_with'):
                regex = "^%s.*?" % val
                regex = compile(regex) if key_split[1] == \
                                          "starts_with" else \
                    compile(regex, IGNORECASE)
                updated_query.update({key_split[0]: regex})

            elif key_split[1] in ('ends_with', 'iends_with'):
                regex = ".*?%s" % val
                regex = compile(regex) if key_split[1] == \
                                          "ends_with" else \
                    compile(regex, IGNORECASE)
                updated_query.update({key_split[0]: regex})

            elif key_split[1] in ("exact", "iexact"):
                regex = "^%s$" % val
                regex = compile(regex) if \
                    key_split[1] == "exact" else \
                    compile(regex, IGNORECASE)
                updated_query.update({key_split[0]: regex})

            elif key_split[1] == 'in':
                updated_query.update({
                    key_split[0]: {"$in": val}})

            else:
                updated_query.update({key: val})

        return updated_query


class ResultSet(object):
    """Resultset"""

    @get_connection
    def __init__(self, iterable, query, cls):
        """
        Resultset init
        :param iterable: Iterable result set/Pymongo cursor
        :return: Result set generator object
        """
        self.item = None
        self.model = cls if isinstance(cls, str) else cls.__class__ \
            .__name__ if cls.__class__.__name__ != 'type' else cls \
            .__name__
        self.pre_filter_query = self.query = query
        self.iterable = iterable if isinstance(
            iterable, (list, tuple, Cursor)) or \
            hasattr(iterable, 'send') else()

    def __getitem__(self, key):
        """Iterator"""
        return self.iterable[key]

    def __len__(self):
        """Result set length property"""
        return len(self.iterable)

    @property
    def count(self):
        """Count property for result set"""
        return len(self.iterable)

    def __iter__(self):
        """Iterator method for Resultset"""
        for item in self.iterable:
            self.item = item
            yield self.item

    def __str__(self):
        """String representation"""
        return "ResultSet object for %s (%r)" % (
            self.model, self.query)

    @classmethod
    def _dump_data(cls, obj):
        """Object dumper helper"""
        obj_data = getattr(obj, 'data', {})
        if hasattr(obj, 'pk'):
            obj_data.update({'pk': getattr(obj, 'pk')})

        return {key if key not in ('_id', 'pk') else 'id':
                    cls._dump_data(val) if hasattr(val, 'pk') else
                    str(val) if key in ('_id', 'id', 'pk') else val
                for key, val in obj_data.items()}

    def json(self, as_list=True, as_dict=False, fields=None,
             exclude=None, ref_fields=None):
        """JSON dumper wrapper
        :param as_list: Boolean to return JSON as a list
        :param as_dict: Boolean to return JSON as an indexed key/value
        pair
        """
        fields = fields if isinstance(fields, (list, tuple)) else ()
        exclude = exclude if isinstance(exclude, (list, tuple)) \
            else ()
        ref_fields = ref_fields if isinstance(ref_fields, dict) \
            else {}

        json_data = {idx: self._dump_data(obj) for idx, obj in
                     enumerate(self.iterable)} if as_dict or not \
            as_list else [self._dump_data(obj) for obj in
                          self.iterable]

        if as_dict or not as_list:
            for key, val in json_data.items():
                yield {key: val}
        else:
            for data in json_data:
                yield data

    def filter(self, *args, **kwargs):
        """Resultset filterer"""

        # Construct query from args tuple
        query = {}
        [query.update(x) for x in args if isinstance(x, dict)]

        # Update query if kwargs brings in query parameters
        query.update(kwargs)

        # Update query key/value to replace 'pk' with '_id' and
        # value to use ObjectId for query
        #query = {'_id' if key in ('pk', 'id',) else key: ObjectId(
        #    val) if key in ('pk', 'id',) else val for key, val in
        #         query.items()}

        self.iterable = [obj for obj in self.iterable if all(
            [getattr(obj, key, None) == val for key, val in query
            .items()])]

        self.query = query
        return self

    def delete(self):
        """Queryset deletion helper method"""
        getattr(self, 'db')[self.model].remove(
            generate_mongo_query(self.query))


class MorDoc(object):
    """Mordor base document"""

    @get_connection
    def __init__(self, **kwargs):
        """Mordoc base document init
        :param kwargs: Init kwargs for user data
        """
        self.pk, self._refs = None, {}
        for key, val in getattr(self, 'data').items():
            setattr(self, key, val)
            members = getattr(self, 'members')
            member = members.get(key)
            if member and hasattr(member, 'value'):
                setattr(member, 'value', val)

        collection = getattr(self, 'collection')

        if not collection:
            raise BaseException("No connection object could be "
                                "decoded")

        # Create/update/delete indexes. Should be moved to __init__?
        collection_indexes = collection.index_information() \
            .keys() if collection else {}

        for member_name, member in getattr(self, 'members').items():
            if not isinstance(member, str):
                model_has_unique = getattr(member, 'unique')
                model_has_unique_with = getattr(member, 'unique_with')

                if not model_has_unique:
                    if member_name in collection_indexes:
                        getattr(self, 'collection').drop_index(
                            member_name)

                elif model_has_unique:
                    unique_idx = {
                        'unique': True,
                        'name': member_name
                    }
                    if not member_name in collection_indexes:
                        getattr(self, 'collection').create_index(
                            member_name,
                            30000,
                            **unique_idx)

                if model_has_unique_with:
                    index_name = "%s-%s" % (member_name,
                                            model_has_unique_with)
                    indexes = [(member_name, ASCENDING),
                               (model_has_unique_with, DESCENDING)]
                    unique_idx = {
                        'unique': True,
                        'name': index_name
                    }
                    if not index_name in collection_indexes:
                        getattr(self, 'collection').create_index(
                            indexes,
                            30000,
                            **unique_idx)

                elif not model_has_unique_with:
                    has_collection_index = [x for x in
                                            collection_indexes if
                                            x.startswith(
                                                "%s-" % member_name)]
                    has_collection_index = has_collection_index[0] \
                        if has_collection_index and len(
                        has_collection_index) == 1 else None
                    if has_collection_index:
                        getattr(self, 'collection').drop_index(
                            has_collection_index)

        document_name = self.__class__.__name__ if \
            self.__class__ .__name__ != "type" else self.__name__

        doc_reg = globals().get('_document_register', ())
        if document_name not in doc_reg:
            doc_reg += (document_name,)
        globals()['_document_register'] = doc_reg


        # Set an instance property to get collection indices
        self.indices = collection.index_information()

    #@property
    #def data(self):
    #    """MorDoc data property"""
    #    return self._data

    def save(self, **kwargs):
        """Save method for MorDoc
        :type kwargs: dict
        :param kwargs: Extra save signals for PyMongo
        """
        collection = getattr(self, 'collection')
        valid_fields, invalids = {}, {}
        members = getattr(self, 'members', {})

        if not collection:
            raise BaseException("No valid DB connection")

        for field_name, field in members.items():
            field_validator = getattr(field, '_validate', None)
            validator_call = getattr(field_validator, '__call__',
                                     None)
            if validator_call and validator_call():
                valid_fields.update({field_name: field})
            else:
                invalids.update({field_name: field})

        # Presave/update reference fields

        members_copy = {}

        for member_name, member in getattr(self, 'members').items():
            field_type = member.__class__.__name__
            field_type = field_type if field_type != 'type' else \
                member.__name__
            if field_type == 'ReferenceField':
                saved_ref = getattr(member, '_value').save()
                self._refs.update({member_name: saved_ref})
                model = getattr(member, '_model')
                model = model if model else member.__class__.__name
                getattr(self, 'data').update({
                    member_name: DBRef(model,
                                       ObjectId(saved_ref.pk))
                })

            else:
                members_copy.update({member_name: member})

        setattr(self, 'members', members_copy)

        if valid_fields.keys() == members.keys():
            pk = collection.insert(getattr(self, 'data'), **kwargs)
            self.pk = ObjectId(pk)

            for key, val in self._refs.items():
                setattr(self, key, val)

            return self
        else:
            raise BaseException("Invalid data for the following "
                                "field(s). {}".format(
                list(invalids.keys())))

    def update(self, update_data):
        """Update document method for MorDoc
        :param update_data: Updation data dictionary
        """
        if not self.pk:
            raise BaseException("Cannot update an unsaved document")
        getattr(self, 'collection').update({'_id': self.pk},
                                           {'$set': update_data})
        for key, val in update_data.items():
            setattr(self, key, val)
            getattr(self, 'data').update({key: val})
        return self

    def __str__(self):
        """String representation"""
        return "%s object (id: %s)" % (self.__class__.__name__ if
                                       self.__class__.__name__ !=
                                       'type' else self.__name__,
                                       self.pk)

    @classmethod
    @get_connection
    def find(cls, *args, **kwargs):
        """Query method for MorDoc documents
        :param args: Arguments tuple of dictionaries for query
        :param kwargs: Keyword arguments for query
        :rtype : ResultSet instance
        """

        # Construct query from args tuple
        query = {}
        [query.update(x) for x in args if isinstance(x, dict)]

        # Update query if kwargs brings in query parameters
        query.update(kwargs)

        # Update query key/value to replace 'pk' with '_id' and
        # value to use ObjectId for query
        query = {'_id' if key in ('pk', 'id',) else key: ObjectId(
            val) if key in ('pk', 'id',) else val for key, val in
            query.items()}

        collection = getattr(cls, 'collection')
        if not collection:
            raise BaseException("No valid DB connection found")

        updated_query = generate_mongo_query(query)

        result_objects = []
        for obj in collection.find(updated_query):
            ret_obj = cls.__new__(cls)
            for key, val in obj.items():
                if key == '_id':
                    setattr(ret_obj, 'pk', val)
                else:
                    setattr(ret_obj, key, val)
            result_objects.append(ret_obj)
        return ResultSet(iterable=result_objects,
                         query=query,
                         cls=cls)

    @classmethod
    @get_connection
    def json(cls, as_list=True, as_dict=False):
        """JSON dumper
        :param as_list: Return JSON as a list object
        :param as_dict: Return JSON as a dict object
        """
        keys = list(getattr(cls, 'data').keys())
        keys.extend(('pk',))
        #return {key: getattr(getattr(cls, key),
        #                     'value', cls.data.get(key, None))
        #        for key in keys}

    @classmethod
    @get_connection
    def objects(cls, *args, **kwargs):
        """
        Objects for model.
        :param args: Arguments tuple. Takes dictionaries for query
        :param kwargs: Keyword arguments tuple. Takes dictionaries for
        query
        """
        objects = []
        query = get_query(args, kwargs)
        return cls.find(query)


__all__ = ('MorDoc', '_document_register')
