# coding=utf-8
"""Mordor package"""
__author__ = 'sriramm'

from .document import MorDoc
from .fields import *
from .utils import connect
