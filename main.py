# coding=utf-8
"""mordor main"""
__author__ = 'sriramm'

import sys
sys.dont_write_bytecode = True
import mordor as md

class AddressDocument(md.MorDoc):
    """Address document"""
    street = md.StringField(unique=False)


class UserDocument(md.MorDoc):
    """User document"""
    username = md.StringField()
    address = md.ReferenceField('AddressDocument')


if __name__ == '__main__':
    md.connect(port=8800, db='testdb')
    UserDocument.objects().delete()
    addr = AddressDocument(street="random street")
    u = UserDocument(username='test user', address=addr)
    u.save()
    print(u.address)
