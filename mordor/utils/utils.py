# coding=utf-8
"""utils module"""
__author__ = 'sriramm'

import sys

sys.dont_write_bytecode = True

from pymongo.errors import AutoReconnect, ConnectionFailure
from pymongo import Connection
from functools import wraps
import logging
import inspect

connections = ()
logger = logging.Logger(__name__)

members = ('collection', 'connection', 'db', '_kwargs', 'data',
           'members', 'collection_name')
methods = ('save', 'update', 'objects', 'find', 'delete', 'json')
excludes = members + methods


def connect(host='localhost', port=27017, db=None):
    """
    Pymongo connection wrapper helper.
    :param host: Database hostname string
    :type host: str
    :param port: Database port
    :type port: int
    :param db: Database name string
    :type db: str
    """
    try:
        connection = Connection(host, port)
        curr_connections = globals().get('connections')
        curr_connections += ({'db': connection[db],
                              'connection': connection},)
        globals()['connections'] = curr_connections
        return connection
    except (AutoReconnect, ConnectionFailure) as exc:
        logger.warning(exc)


def get_connection(function):
    """Get connection wrapper for MorDoc
    :param function: Function to wrap from Mordoc
    """

    @wraps(function)
    def wrapper(self, *args, **kwargs):
        """Get connection wrapper method
        :param self: MorDoc class
        :param args: MorDoc class/instance method args
        :param kwargs: MorDoc class/instance method kwargs
        """
        db_connections = globals().get('connections')
        connection = db_connections[-1] if db_connections \
            and len(db_connections) > 0 else None

        # Set connection/db/collection attributes
        setattr(self, 'connection',
                connection.get('connection') if connection else None)
        setattr(self, 'db',
                connection.get('db') if connection else None)
        setattr(self, 'collection_name',
                self.__class__.__name__ if
                self.__class__.__name__ !=
                'type' else self.__name__ if connection else
                None)
        setattr(self, 'collection',
                connection.get('db')[
                    getattr(self, 'collection_name')] if
                connection else None)

        self.members = {member[0]: getattr(self, member[0]) for
                        member in inspect.getmembers(self)
                        if not (member[0].startswith('__') or
                                member[0].endswith('__')) and
                           member[0] not in excludes}
        self._kwargs = kwargs

        self.data = {key: self._kwargs.get(
            key, getattr(val, 'value', None)) for
                     key, val in self.members.items()}

        return function(self, *args, **kwargs)

    return wrapper


__all__ = ('connect', 'connections', 'get_connection')